#pragma once
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include "weight.h"
#include <fstream>

int hint_in;
int hint_out;
int evstate = 0;
//int bc = 1;
int bonuscount = 0;

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};

/**
 * base agent for agents with weight table
 */
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args) , alpha(0.1f) {
		if(meta.find("init") != meta.end()) //pass init=... to initialize the weight
			init_weights(meta["init"]);
		if(meta.find("load") != meta.end()) //pass load=... to load from a specific file
			load_weights(meta["load"]);
		if(meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~weight_agent() {
		if(meta.find("save") != meta.end()){ //pass save=... to save to a specific file
			save_weights(meta["save"]);
		}
	}

protected:
	virtual void init_weights(const std::string& info) {
		net.emplace_back(11390625); //create an empty weight table with size 50625
		net.emplace_back(11390625); //create an empty weight table with size 50625
		// now net.size() == 2; net[0].size() == 50625; net[1].size() == 50625
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) std::exit(-1);
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
	float alpha;
};

/**
 * base agent for agents with a learning rate
 */
class learning_agent : public agent {
public:
	learning_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if(meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~learning_agent() {}

protected:
	float alpha;
};

/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public weight_agent {
public:
	rndenv(const std::string& args = "") : weight_agent("name=random role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }), bag({1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3}),
		pos0({12, 13, 14, 15}), pos1({0, 1, 2, 3}), pos2({3, 7, 11, 15}), pos3({0, 4, 8, 12}) {
		std::shuffle(bag.begin(), bag.end(), engine); unsigned seed = (unsigned)time(NULL); srand(seed);
		first = 1; pre_v = 0;
	}

	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		std::shuffle(pos0.begin(), pos0.end(), engine);
		std::shuffle(pos1.begin(), pos1.end(), engine);
		std::shuffle(pos2.begin(), pos2.end(), engine);
		std::shuffle(pos3.begin(), pos3.end(), engine);

		min = -10000000;
		board::cell tile;
		//get tile from hint
		if(after.initcount == 0){
			hint_out = bag[0];
			tile = hint_out;
		}
		else{
			if(hint_out == 4){
				tile = (rand()%(after.maxtile - 6)) + 4;
			}
			else{
				tile = hint_out;
			}
		}
		//put next hint
		if(after.maxtile < 7 || rand()%21 || (bonuscount + 1) * 21 > after.initcount){
			if(hint_out == 4){
				if(after.bagcount == 0)
					hint_out = bag[11];
				else
					hint_out = bag[after.bagcount - 1];
			}
			else{
				hint_out = bag[after.bagcount];
				if(after.bagcount == 11){
					std::shuffle(bag.begin(), bag.end(), engine);
				}
			}
		}
		else{
			hint_out = 4;
			bonuscount++;
		}
				
		
		
		if(after.initcount == 0){
			for(int pos : space){
				board b = after;
				if(b(pos) != 0)continue;
				b.place(pos, tile);
				value = 0;
				std::array<float, 32> feature;
				feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
				feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
				feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
				feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
				feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
				feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
				feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
				feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
				feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
				feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
				feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
				feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
				feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
				feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
				feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
				feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
				feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
				feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
				feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
				feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
				feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
				feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
				feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
				feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
				feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
				feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
				feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
				feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
				feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
				feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
				feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
				feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
				for(float f : feature) value = value + f;
				//value = value - plreward;
				if(value > min){
					min = value;
					next_p = pos;
				}
			}
		}
		else if(after.initcount < 9){
			for(int pos : space){
				board b = after;
				if(b(pos) != 0)continue;
				b.place(pos, tile);
				value = 0;
				std::array<float, 32> feature;
				feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
				feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
				feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
				feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
				feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
				feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
				feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
				feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
				feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
				feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
				feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
				feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
				feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
				feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
				feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
				feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
				feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
				feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
				feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
				feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
				feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
				feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
				feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
				feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
				feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
				feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
				feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
				feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
				feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
				feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
				feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
				feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
				for(float f : feature) value = value + f;
				//value = value - plreward;
				max = 0;
				if(after.initcount == 9){
					
					for(int op = 0; op <= 3; op++){
						board c = b;
						board::reward reward = c.slide(op);
						if(reward > max)
							max = reward;
					}
					value = value - max;
				}
				
				if(value > min){
					min = value;
					next_p = pos;
				}
			}
		}
		else{
			if(after.direction == 0){
				for(int pos : pos0){
					board b = after;
					if(b(pos) != 0)continue;
					b.place(pos, tile);
					value = 0;
					std::array<float, 32> feature;
					feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
					feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
					feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
					feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
					feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
					feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
					feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
					feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
					feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
					feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
					feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
					feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
					feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
					feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
					feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
					feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
					feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
					feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
					feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
					feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
					feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
					feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
					feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
					feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
					feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
					feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
					feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
					feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
					feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
					feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
					feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
					feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
					for(float f : feature) value = value + f;
					//value = value - plreward;
					
					max = 0;
					
					for(int op = 0; op <= 3; op++){
						board c = b;
						board::reward reward = c.slide(op);
						if(reward > max)
							max = reward;
					}
					value = value - max;
					
					if(value > min){
						min = value;
						next_p = pos;
					}
				}
			}
			else if(after.direction == 1){
				for(int pos : pos1){
					board b = after;
					if(b(pos) != 0)continue;
					b.place(pos, tile);
					value = 0;
					std::array<float, 32> feature;
					feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
					feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
					feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
					feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
					feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
					feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
					feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
					feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
					feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
					feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
					feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
					feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
					feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
					feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
					feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
					feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
					feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
					feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
					feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
					feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
					feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
					feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
					feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
					feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
					feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
					feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
					feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
					feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
					feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
					feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
					feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
					feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
					for(float f : feature) value = value + f;
					//value = value - plreward;
					
					max = 0;
					for(int op = 0; op <= 3; op++){
						board c = b;
						board::reward reward = c.slide(op);
						if(reward > max)
							max = reward;
					}
					value = value - max;
					
					if(value > min){
						min = value;
						next_p = pos;
					}
				}
			}
			else if(after.direction == 2){
				for(int pos : pos2){
					board b = after;
					if(b(pos) != 0)continue;
					b.place(pos, tile);
					value = 0;
					std::array<float, 32> feature;
					feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
					feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
					feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
					feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
					feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
					feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
					feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
					feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
					feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
					feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
					feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
					feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
					feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
					feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
					feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
					feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
					feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
					feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
					feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
					feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
					feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
					feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
					feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
					feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
					feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
					feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
					feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
					feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
					feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
					feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
					feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
					feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
					for(float f : feature) value = value + f;
					//value = value - plreward;
					
					max = 0;
					
					for(int op = 0; op <= 3; op++){
						board c = b;
						board::reward reward = c.slide(op);
						if(reward > max)
							max = reward;
					}
					value = value - max;
					
					if(value > min){
						min = value;
						next_p = pos;
					}
				}
			}
			else if(after.direction == 3){
				for(int pos : pos3){
					board b = after;
					if(b(pos) != 0)continue;
					b.place(pos, tile);
					value = 0;
					std::array<float, 32> feature;
					feature[0] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(3)*225+b(4)*15+b(5)];
					feature[1] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(15)*225+b(2)*15+b(6)];
					feature[2] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(12)*225+b(11)*15+b(10)];
					feature[3] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(0)*225+b(13)*15+b(9)];
					feature[4] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(0)*225+b(7)*15+b(6)];
					feature[5] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(12)*225+b(1)*15+b(5)];
					feature[6] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(15)*225+b(8)*15+b(9)];
					feature[7] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(3)*225+b(14)*15+b(10)];
					feature[8] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(7)*225+b(8)*15+b(9)];
					feature[9] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(14)*225+b(1)*15+b(5)];
					feature[10] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(8)*225+b(7)*15+b(6)];
					feature[11] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(1)*225+b(14)*15+b(10)];
					feature[12] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(4)*225+b(11)*15+b(10)];
					feature[13] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(13)*225+b(2)*15+b(6)];
					feature[14] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(11)*225+b(4)*15+b(5)];
					feature[15] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(2)*225+b(13)*15+b(9)];
					feature[16] = net[1][b(0)*759375+b(1)*50625+b(2)*3375+b(4)*225+b(5)*15+b(6)];
					feature[17] = net[1][b(3)*759375+b(7)*50625+b(11)*3375+b(2)*225+b(6)*15+b(10)];
					feature[18] = net[1][b(15)*759375+b(14)*50625+b(13)*3375+b(11)*225+b(10)*15+b(9)];
					feature[19] = net[1][b(12)*759375+b(8)*50625+b(4)*3375+b(13)*225+b(9)*15+b(5)];
					feature[20] = net[1][b(3)*759375+b(2)*50625+b(1)*3375+b(7)*225+b(6)*15+b(5)];
					feature[21] = net[1][b(0)*759375+b(4)*50625+b(8)*3375+b(1)*225+b(5)*15+b(9)];
					feature[22] = net[1][b(12)*759375+b(13)*50625+b(14)*3375+b(8)*225+b(9)*15+b(10)];
					feature[23] = net[1][b(15)*759375+b(11)*50625+b(7)*3375+b(14)*225+b(10)*15+b(6)];
					feature[24] = net[1][b(4)*759375+b(5)*50625+b(6)*3375+b(8)*225+b(9)*15+b(10)];
					feature[25] = net[1][b(2)*759375+b(6)*50625+b(10)*3375+b(1)*225+b(5)*15+b(9)];
					feature[26] = net[1][b(11)*759375+b(10)*50625+b(9)*3375+b(7)*225+b(6)*15+b(5)];
					feature[27] = net[1][b(13)*759375+b(9)*50625+b(5)*3375+b(14)*225+b(10)*15+b(6)];
					feature[28] = net[1][b(7)*759375+b(6)*50625+b(5)*3375+b(11)*225+b(10)*15+b(9)];
					feature[29] = net[1][b(1)*759375+b(5)*50625+b(9)*3375+b(2)*225+b(6)*15+b(10)];
					feature[30] = net[1][b(8)*759375+b(9)*50625+b(10)*3375+b(4)*225+b(5)*15+b(6)];
					feature[31] = net[1][b(14)*759375+b(10)*50625+b(6)*3375+b(13)*225+b(9)*15+b(5)];
					for(float f : feature) value = value + f;
					//value = value - plreward;
					
					max = 0;
					
					for(int op = 0; op <= 3; op++){
						board c = b;
						board::reward reward = c.slide(op);
						if(reward > max)
							max = reward;
					}
					value = value - max;
					
					if(value > min){
						min = value;
						next_p = pos;
					}
				}
			}
		}
		
		if(min != -10000000){
			board tmp_b = after;
			tmp_b.place(next_p, tile);
			if(evstate >= 1 && meta.find("save") != meta.end()){
				float delta = alpha*(min - pre_v);
				net[1][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(3)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(15)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(12)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(0)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(0)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(12)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(15)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(3)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(7)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(14)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(8)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(1)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(4)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(13)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(11)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(2)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
				net[1][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
				net[1][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
				net[1][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
				net[1][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
			}
			//pre_v = min + plreward;
			pre_v = min + max;
			pre_b = tmp_b;
			evstate++;
			
			return action::place(next_p, tile);
		}
		return action::place(next_p, tile);
		
		return action();
	}

private:
	std::array<int, 16> space;
	std::array<int, 12> bag;
	std::array<int, 4> pos0;
	std::array<int, 4> pos1;
	std::array<int, 4> pos2;
	std::array<int, 4> pos3;
	int first;
	int next_p;
	float value;
	float min;
	float max;
	float pre_v;
	board pre_b;
	std::default_random_engine engine;
};

/**
 * dummy player
 * select a legal action randomly
 */
class player : public weight_agent {
public:
	player(const std::string& args = "") : weight_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }) { state = 0; pre_v = 0;}

	virtual action take_action(const board& before) {
		max = -10000000;
		for (int op : opcode) {
			board evalu_b = before;
			value = 0;
			board::reward reward = evalu_b.slide(op);
			if(reward != -1){
				feature[0] = net[0][evalu_b(0)*759375+evalu_b(1)*50625+evalu_b(2)*3375+evalu_b(3)*225+evalu_b(4)*15+evalu_b(5)];
				feature[1] = net[0][evalu_b(3)*759375+evalu_b(7)*50625+evalu_b(11)*3375+evalu_b(15)*225+evalu_b(2)*15+evalu_b(6)];
				feature[2] = net[0][evalu_b(15)*759375+evalu_b(14)*50625+evalu_b(13)*3375+evalu_b(12)*225+evalu_b(11)*15+evalu_b(10)];
				feature[3] = net[0][evalu_b(12)*759375+evalu_b(8)*50625+evalu_b(4)*3375+evalu_b(0)*225+evalu_b(13)*15+evalu_b(9)];
				feature[4] = net[0][evalu_b(3)*759375+evalu_b(2)*50625+evalu_b(1)*3375+evalu_b(0)*225+evalu_b(7)*15+evalu_b(6)];
				feature[5] = net[0][evalu_b(0)*759375+evalu_b(4)*50625+evalu_b(8)*3375+evalu_b(12)*225+evalu_b(1)*15+evalu_b(5)];
				feature[6] = net[0][evalu_b(12)*759375+evalu_b(13)*50625+evalu_b(14)*3375+evalu_b(15)*225+evalu_b(8)*15+evalu_b(9)];
				feature[7] = net[0][evalu_b(15)*759375+evalu_b(11)*50625+evalu_b(7)*3375+evalu_b(3)*225+evalu_b(14)*15+evalu_b(10)];
				feature[8] = net[0][evalu_b(4)*759375+evalu_b(5)*50625+evalu_b(6)*3375+evalu_b(7)*225+evalu_b(8)*15+evalu_b(9)];
				feature[9] = net[0][evalu_b(2)*759375+evalu_b(6)*50625+evalu_b(10)*3375+evalu_b(14)*225+evalu_b(1)*15+evalu_b(5)];
				feature[10] = net[0][evalu_b(11)*759375+evalu_b(10)*50625+evalu_b(9)*3375+evalu_b(8)*225+evalu_b(7)*15+evalu_b(6)];
				feature[11] = net[0][evalu_b(13)*759375+evalu_b(9)*50625+evalu_b(5)*3375+evalu_b(1)*225+evalu_b(14)*15+evalu_b(10)];
				feature[12] = net[0][evalu_b(7)*759375+evalu_b(6)*50625+evalu_b(5)*3375+evalu_b(4)*225+evalu_b(11)*15+evalu_b(10)];
				feature[13] = net[0][evalu_b(1)*759375+evalu_b(5)*50625+evalu_b(9)*3375+evalu_b(13)*225+evalu_b(2)*15+evalu_b(6)];
				feature[14] = net[0][evalu_b(8)*759375+evalu_b(9)*50625+evalu_b(10)*3375+evalu_b(11)*225+evalu_b(4)*15+evalu_b(5)];
				feature[15] = net[0][evalu_b(14)*759375+evalu_b(10)*50625+evalu_b(6)*3375+evalu_b(2)*225+evalu_b(13)*15+evalu_b(9)];
				feature[16] = net[0][evalu_b(0)*759375+evalu_b(1)*50625+evalu_b(2)*3375+evalu_b(4)*225+evalu_b(5)*15+evalu_b(6)];
				feature[17] = net[0][evalu_b(3)*759375+evalu_b(7)*50625+evalu_b(11)*3375+evalu_b(2)*225+evalu_b(6)*15+evalu_b(10)];
				feature[18] = net[0][evalu_b(15)*759375+evalu_b(14)*50625+evalu_b(13)*3375+evalu_b(11)*225+evalu_b(10)*15+evalu_b(9)];
				feature[19] = net[0][evalu_b(12)*759375+evalu_b(8)*50625+evalu_b(4)*3375+evalu_b(13)*225+evalu_b(9)*15+evalu_b(5)];
				feature[20] = net[0][evalu_b(3)*759375+evalu_b(2)*50625+evalu_b(1)*3375+evalu_b(7)*225+evalu_b(6)*15+evalu_b(5)];
				feature[21] = net[0][evalu_b(0)*759375+evalu_b(4)*50625+evalu_b(8)*3375+evalu_b(1)*225+evalu_b(5)*15+evalu_b(9)];
				feature[22] = net[0][evalu_b(12)*759375+evalu_b(13)*50625+evalu_b(14)*3375+evalu_b(8)*225+evalu_b(9)*15+evalu_b(10)];
				feature[23] = net[0][evalu_b(15)*759375+evalu_b(11)*50625+evalu_b(7)*3375+evalu_b(14)*225+evalu_b(10)*15+evalu_b(6)];
				feature[24] = net[0][evalu_b(4)*759375+evalu_b(5)*50625+evalu_b(6)*3375+evalu_b(8)*225+evalu_b(9)*15+evalu_b(10)];
				feature[25] = net[0][evalu_b(2)*759375+evalu_b(6)*50625+evalu_b(10)*3375+evalu_b(1)*225+evalu_b(5)*15+evalu_b(9)];
				feature[26] = net[0][evalu_b(11)*759375+evalu_b(10)*50625+evalu_b(9)*3375+evalu_b(7)*225+evalu_b(6)*15+evalu_b(5)];
				feature[27] = net[0][evalu_b(13)*759375+evalu_b(9)*50625+evalu_b(5)*3375+evalu_b(14)*225+evalu_b(10)*15+evalu_b(6)];
				feature[28] = net[0][evalu_b(7)*759375+evalu_b(6)*50625+evalu_b(5)*3375+evalu_b(11)*225+evalu_b(10)*15+evalu_b(9)];
				feature[29] = net[0][evalu_b(1)*759375+evalu_b(5)*50625+evalu_b(9)*3375+evalu_b(2)*225+evalu_b(6)*15+evalu_b(10)];
				feature[30] = net[0][evalu_b(8)*759375+evalu_b(9)*50625+evalu_b(10)*3375+evalu_b(4)*225+evalu_b(5)*15+evalu_b(6)];
				feature[31] = net[0][evalu_b(14)*759375+evalu_b(10)*50625+evalu_b(6)*3375+evalu_b(13)*225+evalu_b(9)*15+evalu_b(5)];
				for(float f : feature) value = value + f;
				value = value + reward;
			
				if(value > max) {
					max = value;
					next_a = op;
				}
			}
		}
		if(max != -10000000) {
			//learning
			board tmp_b = before;
			board::reward reward = tmp_b.slide(next_a);
			if(state >= 1 && meta.find("save") != meta.end()) {
				float delta = alpha*(max - pre_v);
				net[0][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(3)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(15)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(12)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(0)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(0)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(12)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(15)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(3)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(7)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(14)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(8)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(1)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(4)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(13)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(11)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(2)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
				net[0][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
				net[0][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
				net[0][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
				net[0][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
			}
			pre_v = max - reward;
			pre_b = tmp_b;
			state++;
			return action::slide(next_a);
		}
		else {
			float delta = alpha*(0 - pre_v);
			net[0][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(3)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(15)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(12)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(0)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(0)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(12)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(15)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(3)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(7)*225+pre_b(8)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(14)*225+pre_b(1)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(8)*225+pre_b(7)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(1)*225+pre_b(14)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(4)*225+pre_b(11)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(13)*225+pre_b(2)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(11)*225+pre_b(4)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(2)*225+pre_b(13)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(0)*759375+pre_b(1)*50625+pre_b(2)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(3)*759375+pre_b(7)*50625+pre_b(11)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(15)*759375+pre_b(14)*50625+pre_b(13)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(12)*759375+pre_b(8)*50625+pre_b(4)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(3)*759375+pre_b(2)*50625+pre_b(1)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(0)*759375+pre_b(4)*50625+pre_b(8)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(12)*759375+pre_b(13)*50625+pre_b(14)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(15)*759375+pre_b(11)*50625+pre_b(7)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(4)*759375+pre_b(5)*50625+pre_b(6)*3375+pre_b(8)*225+pre_b(9)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(2)*759375+pre_b(6)*50625+pre_b(10)*3375+pre_b(1)*225+pre_b(5)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(11)*759375+pre_b(10)*50625+pre_b(9)*3375+pre_b(7)*225+pre_b(6)*15+pre_b(5)] += (delta / 32);
			net[0][pre_b(13)*759375+pre_b(9)*50625+pre_b(5)*3375+pre_b(14)*225+pre_b(10)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(7)*759375+pre_b(6)*50625+pre_b(5)*3375+pre_b(11)*225+pre_b(10)*15+pre_b(9)] += (delta / 32);
			net[0][pre_b(1)*759375+pre_b(5)*50625+pre_b(9)*3375+pre_b(2)*225+pre_b(6)*15+pre_b(10)] += (delta / 32);
			net[0][pre_b(8)*759375+pre_b(9)*50625+pre_b(10)*3375+pre_b(4)*225+pre_b(5)*15+pre_b(6)] += (delta / 32);
			net[0][pre_b(14)*759375+pre_b(10)*50625+pre_b(6)*3375+pre_b(13)*225+pre_b(9)*15+pre_b(5)] += (delta / 32);
			state = 0;
			evstate = 0;
			bonuscount = 0;
			plreward = 0;
			return action();
		}
	}

private:
	std::array<int, 4> opcode;
	std::array<float, 32> feature;
	int next_a;
	float value;
	float max;
	float pre_v;
	board pre_b;
	int state;
};
